<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderItemRepository")
 */
class OrderItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=38)
     */
    private $guid;

    /**
     * @ORM\Column(type="integer")
     */
    private $visitId;

    /**
     * @ORM\Column(type="integer")
     */
    private $orderId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $finished = false;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MenuItem")
     */
    private $dishes;

    /**
     * OrderItem constructor.
     * @param $guid
     * @param $visitId
     * @param $orderId
     */
    public function __construct(string $guid, int $visitId, int $orderId)
    {
        $this->guid = $guid;
        $this->visitId = $visitId;
        $this->orderId = $orderId;
        $this->dishes = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getVisitId(): int
    {
        return $this->visitId;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getFinished(): bool
    {
        return $this->finished;
    }

    public function setFinished(bool $finished): self
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * @return Collection|MenuItem[]
     */
    public function getDishes(): Collection
    {
        return $this->dishes;
    }

    public function addDish(MenuItem $dish): self
    {
        if (!$this->dishes->contains($dish)) {
            $this->dishes[] = $dish;
        }

        return $this;
    }

    public function removeDish(MenuItem $dish): self
    {
        if ($this->dishes->contains($dish)) {
            $this->dishes->removeElement($dish);
        }

        return $this;
    }
}

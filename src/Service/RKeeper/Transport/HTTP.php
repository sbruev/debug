<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 04.12.2018
 * Time: 11:06
 */

namespace App\Service\RKeeper\Transport;


use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

class HTTP implements TransportInterface
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var string
     */
    private $uri;
    /**
     * @var Auth
     */
    private $auth;

    public function __construct(ClientInterface $client, string $uri, AuthInterface $auth)
    {
        $this->client = $client;
        $this->uri = $uri;
        $this->auth = $auth;
    }

    public function sendXML(string $xml): ResponseInterface
    {
        $request = new Request('POST', $this->uri, ['Content-Type' => 'text/xml'], $xml);
        $response = $this->client->send($request, ['auth' => $this->auth->getAuthArray()]); //TODO: error handling
        return $response;
    }
}
<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181219121047 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE employee_item (id INT AUTO_INCREMENT NOT NULL, guid VARCHAR(38) NOT NULL, ident INT NOT NULL, code INT DEFAULT NULL, name VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu_item (id INT AUTO_INCREMENT NOT NULL, guid VARCHAR(38) NOT NULL, ident INT NOT NULL, ext_code INT NOT NULL, name VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, code INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_item (id INT AUTO_INCREMENT NOT NULL, guid VARCHAR(38) NOT NULL, visit_id INT NOT NULL, order_id INT NOT NULL, finished TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_item_menu_item (order_item_id INT NOT NULL, menu_item_id INT NOT NULL, INDEX IDX_B3FE1310E415FB15 (order_item_id), INDEX IDX_B3FE13109AB44FE0 (menu_item_id), PRIMARY KEY(order_item_id, menu_item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE table_item (id INT AUTO_INCREMENT NOT NULL, guid VARCHAR(38) NOT NULL, ident INT NOT NULL, code INT NOT NULL, name VARCHAR(255) DEFAULT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE waiter (id INT AUTO_INCREMENT NOT NULL, guid VARCHAR(38) NOT NULL, ident INT NOT NULL, code INT DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE order_item_menu_item ADD CONSTRAINT FK_B3FE1310E415FB15 FOREIGN KEY (order_item_id) REFERENCES order_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_item_menu_item ADD CONSTRAINT FK_B3FE13109AB44FE0 FOREIGN KEY (menu_item_id) REFERENCES menu_item (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_item_menu_item DROP FOREIGN KEY FK_B3FE13109AB44FE0');
        $this->addSql('ALTER TABLE order_item_menu_item DROP FOREIGN KEY FK_B3FE1310E415FB15');
        $this->addSql('DROP TABLE employee_item');
        $this->addSql('DROP TABLE menu_item');
        $this->addSql('DROP TABLE order_item');
        $this->addSql('DROP TABLE order_item_menu_item');
        $this->addSql('DROP TABLE table_item');
        $this->addSql('DROP TABLE waiter');
    }
}

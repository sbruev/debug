<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use SimpleXMLElement;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WaiterRepository")
 * @ORM\Table(name="waiter")
 */
class Waiter implements rKeeperConstructInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $guid;

    /**
     * @ORM\Column(type="integer")
     */
    private $ident;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getIdent(): ?int
    {
        return $this->ident;
    }

    public function setIdent(int $ident): self
    {
        $this->ident = $ident;

        return $this;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(?int $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function rKeeperConstruct(SimpleXMLElement $values): void
    {
        $attributes = $values->attributes();
        $this
            ->setGuid((string)$attributes['GUIDString'])
            ->setIdent((int)$attributes['Ident'])
            ->setCode((int) $attributes['Code'])
            ->setName((string)$attributes['Name'])
            ->setType((string)$attributes['Type'])
        ;
    }
}

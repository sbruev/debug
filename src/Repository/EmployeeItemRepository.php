<?php

namespace App\Repository;

use App\Entity\EmployeeItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EmployeeItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmployeeItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmployeeItem[]    findAll()
 * @method EmployeeItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EmployeeItem::class);
    }

    // /**
    //  * @return EmployeeItem[] Returns an array of EmployeeItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmployeeItem
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 04.12.2018
 * Time: 14:36
 */

namespace App\Service\RKeeper\Transport;


use Psr\Http\Message\ResponseInterface;

interface TransportInterface
{
    public function sendXML(string $xml): ResponseInterface;
}
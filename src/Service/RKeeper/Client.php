<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 07.12.2018
 * Time: 19:13
 */

namespace App\Service\RKeeper;


use App\Entity\OrderItem;
use App\Service\RKeeper\Transport\TransportInterface;
use \DOMDocument;
use \SimpleXMLElement;

class Client
{
    const VERSION = '1.0';
    const ENCODING = 'windows-1251';

    private $rootXML;
    private $query;
    /**
     * @var TransportInterface
     */
    private $transport;

    public function __construct(TransportInterface $transport, string $version = null, string $encoding = null)
    {
        $version = $version ?: self::VERSION;
        $encoding = $encoding ?: self::ENCODING;

        $this->rootXML = new DOMDocument($version, $encoding);
        $this->query = $this->rootXML->createElement('RK7Query');
        $this->rootXML->appendChild($this->query);
        $this->transport = $transport;
    }

    public function sendCmd(DOMDocument $xml, string $command, array $options = [], array $children = []): SimpleXMLElement
    {
        $query = $xml->firstChild;
        $cmd = $xml->createElement('RK7CMD');
        $cmd->setAttribute('CMD', $command);

        foreach ($options as $key => $value) {
            $cmd->setAttribute($key, $value);
        }
        $query->appendChild($cmd);

        foreach ($children as $child) {
            $cmd->appendChild($child);
        }
        return $this->sendDOMDocument($xml);
    }

    private function sendDOMDocument(DOMDocument $xml): SimpleXMLElement
    {
        $response = $this->transport->sendXML($xml->saveXML());
        $responseXml = new SimpleXMLElement($response->getBody());

        return $responseXml;
    }

    public function getRefData(string $refName, array $options = []): SimpleXMLElement
    {
        $xml = clone $this->rootXML;
        $RK7QueryResult = $this->sendCmd($xml, 'GetRefData', array_merge(['RefName' => $refName], $options));
        return $RK7QueryResult;
    }

    public function getMenuItems(array $options = [])
    {
        $RK7QueryResult = $this->getRefData('MENUITEMS', $options);
        $RK7Reference = $RK7QueryResult->children();
        $items = $RK7Reference->children();
        return $items->children();
    }

    public function getTableItems(array $options = []): SimpleXMLElement
    {
        $RK7QueryResult = $this->getRefData('TABLES', $options);
        $RK7Reference = $RK7QueryResult->children();
        $items = $RK7Reference->children();
        return $items->children();
    }

    public function getEmployeeItems(array $options = []): SimpleXMLElement
    {
        $RK7QueryResult = $this->getRefData('EMPLOYEES', $options);
        $RK7Reference = $RK7QueryResult->children();
        $items = $RK7Reference->children();
        return $items->children();
    }

    public function getOrderList(array $options = []): SimpleXMLElement
    {
        $xml = clone $this->rootXML;
        $RK7QueryResult = $this->sendCmd($xml, 'GetOrderList', $options);
        return $RK7QueryResult;
    }

    public function openOrder(int $table_ident, int $waiter_ident = null, string $comment = null): SimpleXMLElement
    {
        $xml = clone $this->rootXML;

        $order = $xml->createElement('Order');
        if (!empty($comment)) {
            $order->setAttribute('persistentComment', $comment);
        }

        $table = $xml->createElement('Table');
        $table->setAttribute('id', $table_ident);
        $order->appendChild($table);

        if (!is_null($waiter_ident)) {
            $waiter = $xml->createElement('Waiter');
            $waiter->setAttribute('id', $waiter_ident);
            $order->appendChild($waiter);
        }

        return $this->sendCmd($xml, 'CreateOrder', [], [$order]);
    }

    public function addDishes()
    {
        $xml = clone $this->rootXML;

        $children = array();

        $order = $xml->createElement('Order');
        if (!empty($orderId['guid'])) {
            $order->setAttribute('guid', $orderId['guid']);
        } elseif (!empty($orderId['VisitID']) && !empty($orderId['OrderID'])) {
            $order->setAttribute('visit', $orderId['VisitID']);
            $order->setAttribute('orderIdent', $orderId['OrderID']);
        } else {
            throw new \Exception(); //TODO: wrong order ID
        }
        $children[] = $order;

        $payment = $xml->createElement('Payment');
        $payment->setAttribute('amount', 0);
        $children[] = $payment;

        $RK7QueryResult = $this->sendCmd($xml, 'PayOrder', $options, $children);
        return $RK7QueryResult;
    }

    public function payOrder(OrderItem $orderId, array $options = []): SimpleXMLElement
    {
        $xml = clone $this->rootXML;

        $children = array();

        $order = $xml->createElement('Order');

        $order->setAttribute('guid', $orderId->getGuid());

        $children[] = $order;

        $payment = $xml->createElement('Payment');
        $payment->setAttribute('amount', 0);
        $children[] = $payment;

        $RK7QueryResult = $this->sendCmd($xml, 'PayOrder', $options, $children);
        return $RK7QueryResult;
    }

    public function getSystemInfo()
    {
        $xml = clone $this->rootXML;
        $RK7QueryResult = $this->sendCmd($xml, 'GetSystemInfo');
        return $RK7QueryResult;
    }
}
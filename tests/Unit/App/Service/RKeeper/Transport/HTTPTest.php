<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 04.12.2018
 * Time: 12:26
 */

namespace App\Service\RKeeper\Transport;


use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class HTTPTest extends TestCase
{
    /**
     * @var MockObject|ClientInterface
     */
    private $client;
    /**
     * @var string
     */
    private $uri;
    /**
     * @var Auth
     */
    private $auth;
    /**
     * @var string
     */
    private $xml;

    protected function setUp()
    {
        $this->client = $this->createMock(ClientInterface::class);
        $this->uri = 'http://some.uri';
        $this->auth = new Auth('user', 'pass');
        $this->xml = '<xml></xml>';
    }

    public function testSendXML()
    {
        $request = new Request('POST', $this->uri, ['Content-Type' => 'text/xml'], $this->xml);
        $response = $this->createMock(ResponseInterface::class);

        $this->client->expects($this->once())
            ->method('send')
            ->withConsecutive($request, $this->auth->getAuthArray())
            ->willReturn($response)
        ;

        $http = new HTTP($this->client, $this->uri, $this->auth);
        $this->assertEquals($response, $http->sendXML($this->xml));
    }

    // TODO: test Exceptions
}

<?php

namespace App\Controller;


use App\Entity\EmployeeItem;
use App\Entity\MenuItem;
use App\Entity\OrderItem;
use App\Entity\Restaurant;
use App\Entity\rKeeperConstructInterface;
use App\Entity\TableItem;
use App\Service\RKeeper\Client;
use App\Service\RKeeper\Transport\Auth;
use App\Service\RKeeper\Transport\HTTP;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/vue")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showVue()
    {
        return $this->render('vue.html.twig');
    }

    /**
     * @Route("/api")
     * @param Request $request
     * @return false|string
     */
    public function getApi(Request $request, Client $client)
    {
//        $cmd = $request->query->get('cmd');
//
//        $response = [];
//        $visits = $client->getTableItems();
//        /** @var \SimpleXMLElement $visit */
//        foreach ($visits as $visit) {
//            $response[] = $visit->saveXML();
////            $orders = $visit->xpath('Orders')[0];
////
////            /** @var \SimpleXMLElement $order */
////            foreach ($orders as $order) {
////                $response[] = $visit['VisitID'] . ' - ' . $order['OrderID'];
////            }
//        }
//        $response = array_unique($response);
        $response = $client->getOrderList(['onlyOpened' => true])->saveXML();
        return new JsonResponse($response);
    }

    /**
     * @Route("/menu/fill")
     * @param Client $client
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function fillMenu(Client $client, ObjectManager $objectManager)
    {
        set_time_limit(600);

        /** @var \SimpleXMLElement $collection */
        $collection = $client->getMenuItems();

        $count = $this->fillDatabase($collection, MenuItem::class, $objectManager);

        return $this->render('debug.html.twig', ['messages' => ['done', $count . ' records']]);
    }

    /**
     * @Route("/table/fill")
     * @param Client $client
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function fillTable(Client $client, ObjectManager $objectManager)
    {
        set_time_limit(600);

        /** @var \SimpleXMLElement $collection */
        $collection = $client->getTableItems();

        $count = $this->fillDatabase($collection, TableItem::class, $objectManager);

        return $this->render('debug.html.twig', ['messages' => ['done', $count . ' records']]);
    }

    /**
     * @Route("/waiter/fill")
     *
     * @param Client $client
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function fillEmployee(Client $client, ObjectManager $objectManager)
    {
        set_time_limit(600);

        /** @var \SimpleXMLElement $collection */
        $collection = $client->getEmployeeItems();

        $count = $this->fillDatabase($collection, EmployeeItem::class, $objectManager);

        return $this->render('debug.html.twig', ['messages' => ['done', $count . ' records']]);
    }

    /**
     * @Route("/create")
     *
     * @param Client $client
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createOrder(\GuzzleHttp\Client $guzzle, ObjectManager $objectManager)
    {
        $r_id = 1;
        $restuarant = $objectManager->getRepository(Restaurant::class)->find($r_id);
        $auth = new Auth($restuarant->getUsername(), $restuarant->getPassword());
        $http = new HTTP($guzzle, $restuarant->getUri(), $auth);
        $client = new Client($http);

        $WAITER_IDENT = 1001478;

        // get all reserved tables
        $reserved_tables = [];
        $visits = $client->getOrderList();

        /** @var \SimpleXMLElement $visit */
        foreach ($visits as $visit) {
            $orders = $visit->xpath('Orders')[0];

            /** @var \SimpleXMLElement $order */
            foreach ($orders as $order) {
                $reserved_tables[] = (int)$order['TableCode'];
            }
        }

        $reserved_tables = array_unique($reserved_tables);

        $allTables = $restuarant->getTableItems();

        // get free table
        $table_ident = 0;
        /** @var TableItem $table */
        foreach ($allTables as $table) {
            $table_code = $table->getCode();
            if (!in_array($table_code, $reserved_tables)) {
                $table_ident = $table->getIdent();
                break;
            }
        }
        if ($table_ident === 0) {
            throw new \Exception(); //TODO: no free tables
        }

        // open order

        $response = $client->openOrder($table_ident, $WAITER_IDENT);
        $guid = $response->attributes()['guid'];

        $status = (string)$response->attributes()['Status'];
        $visitId = (int)$response->attributes()['VisitID'];
        $orderId = (int)$response->attributes()['OrderID'];

        $order = new OrderItem($guid, $visitId, $orderId);
        $objectManager->persist($order);
        $objectManager->flush();

        return $this->render('debug.html.twig', ['messages' => [$status, $guid, $visitId, $orderId, $response->saveXML()]]);
    }

    /**
     * @Route("/pay")
     */
    public function payOrder(Client $client, ObjectManager $objectManager)
    {
        /** @var ArrayCollection $orders */
        $orders = $objectManager->getRepository(OrderItem::class)->findAllOpenOrders();
        $order = $orders->first();
        $response = $client->payOrder($order, ['guid' => '{6FA33F83-A5A5-4BFB-8228-2FC4A2745797}']);
        return $this->render('debug.html.twig', ['messages' => [$response->saveXML()]]);
    }

    private function fillDatabase(\SimpleXMLElement $collection, string $entityClass, ObjectManager $objectManager): int
    {

        $batchSize = 10000; //TODO: set optimal batch size

        $count = 0;
        $batchCount = 0;
        $dirty = false;

        /** @var \SimpleXMLElement $collection */
        foreach ($collection as $item) {
            $dirty = true;
            /** @var rKeeperConstructInterface $entity */
            $entity = new $entityClass;
            if (!$entity instanceof rKeeperConstructInterface) {
                throw new \Exception(); //TODO: proper error handling
            }
            $entity->rKeeperConstruct($item);
            $objectManager->persist($entity);
            $batchCount++;
            $count++;

            if ($batchCount > $batchSize) {
                $objectManager->flush();
                $batchCount = 0;
                $dirty = false;
            }
        }
        if ($dirty) {
            $objectManager->flush();
        }

        return $count;
    }


    private function truncateTable(EntityManagerInterface $entityManager, string $entityClassName)
    {
        $cmd = $entityManager->getClassMetadata($entityClassName);
        $connection = $entityManager->getConnection();
        $connection->beginTransaction();

        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $connection->query('DELETE FROM ' . $cmd->getTableName());
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
        $connection->commit();
    }

    private function profileTime(array $dateTime_array): array
    {
        $count = count($dateTime_array);
        foreach ($dateTime_array as $key => $value) {
            if (--$count <= 0) {
                break;
            }

            $time[] = date_diff($value, $dateTime_array[$key + 1])->f * 1000;
        }

        return $time;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 05.12.2018
 * Time: 18:32
 */

namespace App\Service\RKeeper\Transport;


interface AuthInterface
{
    public function getAuthArray(): array;
}
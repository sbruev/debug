<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 10.12.2018
 * Time: 18:41
 */

namespace App\Service\RKeeper;

use App\Service\RKeeper\Transport\TransportInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use \SimpleXMLElement;

class ClientTest extends TestCase
{

    /**
     * @var MockObject|TransportInterface
     */
    private $transport;
    private $command;
    private $options;
    private $version;
    private $encoding;
    private $menu_xml;

    protected function setUp()
    {
        $this->transport = $this->createMock(TransportInterface::class);
        $this->command = 'XXX';
        $this->options = ['filter' => 'true'];
        $this->version = '1.0';
        $this->encoding = Client::ENCODING;

        $this->menu_xml = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<RK7QueryResult ServerVersion="7.4.21.112" XmlVersion="44" Status="Ok" CMD="GetRefData" ErrorText="" DateTime="2013-05-15T19:57:38" WorkTime="0" Processed="1">
	<RK7Reference BlobNames="INSTRUCT,LARGEIMAGEPATH" ClassName="TRK7MenuItems" Name="MENUITEMS" MinIdent="1" MaxIdent="0" ViewRight="0" UpdateRight="21" ChildRight="21" DeleteRight="21" XMLExport="True" XMLMask="*" LeafCollectionCount="1">
		<Items>
			<Item Ident="0" ItemIdent="0" SourceIdent="0" GUIDString="{E501341F-060C-4FC5-B89A-5403095E73E6}" AssignChildsOnServer="False" MainParentIdent="0" ActiveHierarchy="False" Code="0" Name="" AltName="" Status="rsDraft" VisualType_Image="0" VisualType_BColor="536870911" VisualType_TextColor="536870911" VisualType_Flags="bsPictureTop" SalesTerms_Flag="[]" SalesTerms_StartSale="0" SalesTerms_StopSale="0" RightLvl="0" TaxDishType="0" ExtCode="0" ShortName="" AltShortName="" PortionWeight="0" PortionName="" AltPortion="" Kurs="0" QntDecDigits="0" ModiScheme="0" ComboScheme="0" ModiWeight="0" CookMins="0" Comment="" Instruct="" Flags="[]" TaraWeight="0" ConfirmQnt="0" MInterface="0" MinRestQnt="0" BarCodes="" PriceMode="pmPerPiece" OpenPrice="False" DontPack="False" ChangeQntOnce="False" CategPath="" SaleObjectType="sotMenuItem" ComboJoinMode="cjmNone" AddLineMode="almJoinLines" ChangeToCombo="0" GuestsDishRating="300" RateType="rtOneTime" MinimumTarifTime="0" MaximumTarifTime="0" IgnoredTarifTime="0" MinTarifAmount="0" MaxTarifAmount="0" RoundTime="0" TariffRoundRule="trrMath" MoneyRoundRule="rr1Cent" DefTarifTimeLimit="0" ComboDiscount="0" LargeImagePath="" HighLevelGroup1="0" HighLevelGroup2="0" HighLevelGroup3="0" HighLevelGroup4="0" BarCodesText="">
				<RecommendedMenuItems ClassName="TRecommendedMenuItems">
					<Items/>
				</RecommendedMenuItems>
				<Childs ClassName="tChildCollection">
					<Child ChildIdent="1000011" IsTerminal="0"/>
				</Childs>
			</Item>
			<Item Ident="1000011" ItemIdent="1000011" SourceIdent="0" GUIDString="{5B3537CE-ECF0-4D6A-B0AE-F095FC4F0513}" AssignChildsOnServer="False" MainParentIdent="1000010" ActiveHierarchy="True" Code="2" Name="кофе" AltName="" Status="rsActive" VisualType_Image="0" VisualType_BColor="536870911" VisualType_TextColor="536870911" VisualType_Flags="bsPictureTop" SalesTerms_Flag="[]" SalesTerms_StartSale="3577808296547" SalesTerms_StopSale="3577808296547" RightLvl="0" TaxDishType="1" ExtCode="1" ShortName="" AltShortName="" PortionWeight="0" PortionName="" AltPortion="" Kurs="0" QntDecDigits="0" ModiScheme="0" ComboScheme="0" ModiWeight="0" CookMins="0" Comment="" Instruct="" Flags="[]" TaraWeight="0" ConfirmQnt="0" MInterface="0" MinRestQnt="0" BarCodes="" PriceMode="pmPerPiece" OpenPrice="False" DontPack="False" ChangeQntOnce="False" CategPath="Бар" SaleObjectType="sotMenuItem" ComboJoinMode="cjmNone" AddLineMode="almJoinLines" ChangeToCombo="0" GuestsDishRating="300" RateType="rtOneTime" MinimumTarifTime="0" MaximumTarifTime="0" IgnoredTarifTime="0" MinTarifAmount="0" MaxTarifAmount="0" RoundTime="1" TariffRoundRule="trrMath" MoneyRoundRule="rr1Cent" DefTarifTimeLimit="0" ComboDiscount="0" LargeImagePath="" HighLevelGroup1="1000010" HighLevelGroup2="0" HighLevelGroup3="0" HighLevelGroup4="0" BarCodesText="">
				<RecommendedMenuItems ClassName="TRecommendedMenuItems">
					<Items/>
				</RecommendedMenuItems>
				<Childs ClassName="tChildCollection"/>
			</Item>
		</Items>
	</RK7Reference>
</RK7QueryResult>
XML;

    }

    public function testGetRefData()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->expects($this->once())
            ->method('getBody')
            ->willReturn($this->menu_xml);

        $this->transport->expects($this->once())
            ->method('sendXML')
            ->with($this->callback(function ($data) {
                $xml = new \DOMDocument();
                $xml->loadXML($data);
                $first = $xml->firstChild;
                $this->assertEquals('RK7Query', $first->nodeName);
                $command = $first->firstChild;
                $this->assertEquals('RK7CMD', $command->nodeName);
                $options = ['CMD' => 'GetRefData'];
                $options['RefName'] = $this->command;
                foreach ($this->options as $key => $value) {
                    $options[$key] = $value;
                }
                foreach ($options as $key => $value)
                {
                    $this->assertEquals($value, $command->attributes->getNamedItem($key)->textContent);
                }
                return true;
            }))
            ->willReturn($response);
        $simpleXml = new SimpleXMLElement($this->menu_xml);

        $client = new Client($this->transport);

        $this->assertEquals($simpleXml, $client->getRefData($this->command, $this->options));
    }

    public function testSendCmd()
    {
        $version = $this->version;
        $encoding = $this->encoding;

        $simpleXml = new SimpleXMLElement($this->menu_xml);
        $response = $this->createMock(ResponseInterface::class);
        $response->expects($this->once())
            ->method('getBody')
            ->willReturn($this->menu_xml);

        $this->transport->expects($this->once())
            ->method('sendXML')
            ->with($this->callback(function ($data) use ($version, $encoding) {
                $xml = new \DOMDocument();
                $xml->loadXML($data);
                $this->assertEquals($version, $xml->xmlVersion);
                $this->assertEquals($encoding, $xml->xmlEncoding);
                $first = $xml->firstChild;
                $this->assertEquals('RK7Query', $first->nodeName);
                $command = $first->firstChild;
                $this->assertEquals('RK7CMD', $command->nodeName);
                $options = ['CMD' => $this->command];
                foreach ($this->options as $key => $value) {
                    $options[$key] = $value;
                }
                foreach ($options as $key => $value)
                {
                    $this->assertEquals($value, $command->attributes->getNamedItem($key)->textContent);
                }
                return true;
            }))
            ->willReturn($response);
        $client = new Client($this->transport, $version);
        $this->assertEquals($simpleXml, $client->sendCmd($this->command, $this->options));
    }

    public function testGetMenuItems()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->expects($this->once())
            ->method('getBody')
            ->willReturn($this->menu_xml);

        $this->transport->expects($this->once())
            ->method('sendXML')
            ->with($this->callback(function ($data) {
                $xml = new \DOMDocument();
                $xml->loadXML($data);
                $first = $xml->firstChild;
                $this->assertEquals('RK7Query', $first->nodeName);
                $command = $first->firstChild;
                $this->assertEquals('RK7CMD', $command->nodeName);
                $options = ['CMD' => 'GetRefData'];
                $options['RefName'] = 'MENUITEMS';
                foreach ($this->options as $key => $value) {
                    $options[$key] = $value;
                }
                foreach ($options as $key => $value)
                {
                    $this->assertEquals($value, $command->attributes->getNamedItem($key)->textContent);
                }
                return true;
            }))
            ->willReturn($response);
        $simpleXml = new SimpleXMLElement($this->menu_xml);

        // XML structure: RK7QueryResult >> RK7Reference >> Items >> Menuitem[]
        // so, to get collection of items we should move from RK7QueryResult three times
        $menu = $simpleXml->children()->children()->children();

        $client = new Client($this->transport);

        $this->assertEquals($menu, $client->getMenuItems($this->options));
    }
}

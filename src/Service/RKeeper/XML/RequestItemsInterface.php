<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 03.12.2018
 * Time: 19:49
 */

namespace App\Service\RKeeper\XML;

use \DOMNode;

interface RequestItemsInterface
{
    public function getDomNode(): DOMNode;
}
<?php

namespace App\Service\RKeeper\XML;

use \SimpleXMLElement;

class Request
{
    const VERSION = '1.0';
    const ENCODING = 'windows-1251';

    /**
     * @var \DOMDocument
     */
    private $root;
    private $query;

    public function __construct(string $version = null, string $encoding = null)
    {
        $version = $version?:self::VERSION;
        $encoding = $encoding?:self::ENCODING;

        $this->root = new \DOMDocument($version, $encoding);
        $this->query = $this->root->createElement('RK7Query');
        $this->root->appendChild($this->query);
    }

    public function addRK7CMD(array $attributes = null): void
    {
        $cmd = $this->root->createElement('RK7CMD');
        foreach ($attributes as $key => $value) {
            $cmd->setAttribute($key, $value);
        }
        $this->query->appendChild($cmd);
    }

    public function addRK7Command(array $attributes = null, RequestItemsInterface $items = null): void
    {
        $cmd = $this->root->createElement('RK7Command');
        foreach ($attributes as $key => $value) {
            $cmd->setAttribute($key, $value);
        }

        $cmd->appendChild($items->getDomNode());
        $this->query->appendChild($cmd);
    }

    public function asXml()
    {
        return $this->root->saveXML();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 04.12.2018
 * Time: 12:49
 */

namespace App\Service\RKeeper\XML;


use App\Service\RKeeper\Transport\TransportInterface;
use \DOMDocument;
use \SimpleXMLElement;

class Client
{
    const VERSION = '1.0';
    const ENCODING = 'windows-1251';

    private $rootXML;
    private $query;
    /**
     * @var TransportInterface
     */
    private $transport;

    public function __construct(TransportInterface $transport, string $version = null, string $encoding = null)
    {
        $version = $version?:self::VERSION;
        $encoding = $encoding?:self::ENCODING;

        $this->rootXML = new DOMDocument($version, $encoding);
        $this->query = $this->rootXML->createElement('RK7Query');
        $this->rootXML->appendChild($this->query);
        $this->transport = $transport;
    }

    public function sendCmd(string $command, array $options = []): SimpleXMLElement
    {
        $xml = clone $this->rootXML;
        $query = $xml->firstChild;
        $cmd = $xml->createElement('RK7CMD');
        $cmd->setAttribute('CMD', $command);

        foreach ($options as $key => $value)
        {
            $cmd->setAttribute($key, $value);
        }
        $query->appendChild($cmd);

        return $this->sendDOMDocument($xml);
    }

    private function sendDOMDocument(DOMDocument $xml): SimpleXMLElement
    {
        $response = $this->transport->sendXML($xml->saveXML());
        $responseXml = new SimpleXMLElement($response->getBody());

        return $responseXml;
    }

    public function getRefData(string $refName, array $options = []): SimpleXMLElement
    {
        $RK7QueryResult = $this->sendCmd('GetRefData', array_merge(['RefName' => $refName], $options));
        return $RK7QueryResult;
    }

    public function getMenuItems(array $options = []): SimpleXMLElement
    {
        $RK7QueryResult = $this->getRefData('MENUITEMS', $options);
        $RK7Reference = $RK7QueryResult->children();
        $items = $RK7Reference->children();
        return $items->children();
    }

    public function getTablesItems(array $options = []): SimpleXMLElement
    {
        $RK7QueryResult = $this->getRefData('TABLES', $options);
        $RK7Reference = $RK7QueryResult->children();
        $items = $RK7Reference->children();
        return $items->children();
    }
}
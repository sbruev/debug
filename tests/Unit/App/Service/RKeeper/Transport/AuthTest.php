<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 04.12.2018
 * Time: 12:25
 */

namespace App\Service\RKeeper\Transport;


use PHPUnit\Framework\TestCase;

class AuthTest extends TestCase
{
    private $username;
    private $password;

    protected function setUp()
    {
        $seed = rand(1,100);
        $this->username = 'user.' . $seed;
        $this->password = 'password.' . $seed;
    }

    public function testGetAuthArray()
    {
        $auth = new Auth($this->username, $this->password);
        $this->assertEquals([$this->username, $this->password], $auth->getAuthArray());
    }
}

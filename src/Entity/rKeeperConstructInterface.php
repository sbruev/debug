<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 11.12.2018
 * Time: 14:50
 */

namespace App\Entity;

use \SimpleXMLElement;

interface rKeeperConstructInterface
{
    public function rKeeperConstruct(SimpleXMLElement $values): void;
}
<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 04.12.2018
 * Time: 11:23
 */

namespace App\Service\RKeeper\Transport;


class Auth implements AuthInterface
{
    private $auth;

    public function __construct(string $user, string $password)
    {
        $this->auth = [$user, $password];
    }

    public function getAuthArray(): array
    {
        return $this->auth;
    }
}
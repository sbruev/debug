<?php

namespace App\Repository;

use App\Entity\TableItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TableItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableItem[]    findAll()
 * @method TableItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TableItem::class);
    }

    // /**
    //  * @return TableItem[] Returns an array of TableItem objects
    //  */

    public function findDeliveryTables()
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.code BETWEEN :min AND :max')
            ->andWhere('t.status = :status')
            ->setParameters(['min' => 9000, 'max' => 9999, 'status' => 'rsActive'])
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?TableItem
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use SimpleXMLElement;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeItemRepository")
 */
class EmployeeItem implements rKeeperConstructInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=38)
     */
    private $guid;

    /**
     * @ORM\Column(type="integer")
     */
    private $ident;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getIdent(): ?int
    {
        return $this->ident;
    }

    public function setIdent(int $ident): self
    {
        $this->ident = $ident;

        return $this;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(?int $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function rKeeperConstruct(SimpleXMLElement $values): void
    {
        $attributes = $values->attributes();
        $this
            ->setGuid((string)$attributes['GUIDString'])
            ->setIdent((int)$attributes['Ident'])
            ->setCode((int)$attributes['Code'])
            ->setName((string)$attributes['Name'])
            ->setStatus((string)$attributes['Status'])
        ;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: sbruev_i
 * Date: 13.12.2018
 * Time: 18:08
 */

namespace App\Controller;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class rest extends AbstractController
{
    /**
     * @Route("/parse_day")
     */
    public function parse_day(EntityManagerInterface $manager)
    {
        set_time_limit(600);

        $priority = 0;
        $min_cost = 100;
        $start_time = 480;
        $end_time = 1200;

        $raw = file_get_contents(__dir__ . '/../tanuki_daytime.json');

        $input = json_decode($raw, true);
        $collection = $input['features'];
        $response = array();
        foreach ($collection as $key => $zone) {
            $g = array();
            $geometry = $zone['geometry'];
            if ($geometry['type'] !== 'LineString') {
                continue;
            }
            $coordinates = $geometry['coordinates'];
            $first = $coordinates[0][1] . ' ' . $coordinates[0][0];
            foreach ($coordinates as $coordinate) {
                $g[] = $coordinate[1] . ' ' . $coordinate[0];
            }
            $g[] = $first;
            $geo = implode(',', $g);

            $property = $zone['properties'];
            $name = $property['description'];

            if ($name === 'Территория Балаклавского проспекта, передана в постоянное обслуживание ресторану Шаболовка') {
                continue;
            }

            if (array_key_exists('stroke', $property)) {
                $color = $property['stroke'];
            } else {
                $color = $property['marker-color'];
            }
            switch ($color) {
                case '#0e4779':
                    $lead_time = 90;
                    break;
                case '#ed4543':
                    $lead_time = 45;
                    break;
                case '#b51eff':
                    $lead_time = 60;
                    break;
                case '#1bad03':
                    $lead_time = 120;
                    break;
                default:
                    dump($name, $color, $geometry['type']);
                    throw new \Exception('missing color');
            }


            $manager->getConnection()->prepare("INSERT INTO delivery_zone (zone, minimal_cost, lead_time, priority, start_time, end_time, zone_name)
VALUES (ST_GeomFromText('POLYGON(($geo))'), $min_cost, $lead_time, $priority, $start_time, $end_time, \"$name\")")
                ->execute();
        }
        return $this->render('debug.html.twig', ['messages' => $response]);
    }

    /**
     * @Route("/parse_night")
     */
    public function parse_night(EntityManagerInterface $manager)
    {
        set_time_limit(600);

        $priority = 10;
        $min_cost = 100;
        $lead_time = 60;
        $start_time = 0;
        $end_time = 1439;

        $raw = file_get_contents(__dir__ . '/../tanuki_night.json');

        $input = json_decode($raw, true);
        $collection = $input['features'];
        $response = array();
        foreach ($collection as $key => $zone) {
            $g = array();
            $geometry = $zone['geometry'];
            if ($geometry['type'] !== 'LineString') {
                continue;
            }
            $coordinates = $geometry['coordinates'];
            $first = $coordinates[0][1] . ' ' . $coordinates[0][0];
            foreach ($coordinates as $coordinate) {
                $g[] = $coordinate[1] . ' ' . $coordinate[0];
            }
            $g[] = $first;
            $geo = implode(',', $g);

            $property = $zone['properties'];
            if (!array_key_exists('description', $property)) {
                continue;
            }
            $name = $property['description'];

            $manager->getConnection()->prepare("INSERT INTO delivery_zone (zone, minimal_cost, lead_time, priority, start_time, end_time, zone_name)
VALUES (ST_GeomFromText('POLYGON(($geo))'), $min_cost, $lead_time, $priority, $start_time, $end_time, \"$name\")")
                ->execute();
        }
        return $this->render('debug.html.twig', ['messages' => $response]);
    }

    /**
     * @Route("/parse_ersh")
     */
    public function parse_ersh(EntityManagerInterface $manager)
    {
        set_time_limit(600);

        $priority = 0;
        $min_cost = 100;
        $start_time = 0;
        $end_time = 1439;

        $raw = file_get_contents(__dir__ . '/../ersh.json');

        $input = json_decode($raw, true);
        $collection = $input['features'];
        $response = array();
        foreach ($collection as $key => $zone) {
            $g = array();
            $geometry = $zone['geometry'];
            if ($geometry['type'] !== 'LineString') {
                continue;
            }
            $coordinates = $geometry['coordinates'];
            $first = $coordinates[0][1] . ' ' . $coordinates[0][0];
            foreach ($coordinates as $coordinate) {
                $g[] = $coordinate[1] . ' ' . $coordinate[0];
            }
            $g[] = $first;
            $geo = implode(',', $g);

            $property = $zone['properties'];
            if (!array_key_exists('description', $property)) {
                continue;
            }
            $name = $property['description'];

            if (array_key_exists('stroke', $property)) {
                $color = $property['stroke'];
            } else {
                $color = $property['marker-color'];
            }
            switch ($color) {
                case '#0e4779':
                    $lead_time = 60;
                    break;
                default:
                    $lead_time = 90;
            }


            $manager->getConnection()->prepare("INSERT INTO ersh_zone (zone, minimal_cost, lead_time, priority, start_time, end_time, zone_name)
VALUES (ST_GeomFromText('POLYGON(($geo))'), $min_cost, $lead_time, $priority, $start_time, $end_time, \"$name\")")
                ->execute();
        }
        return $this->render('debug.html.twig', ['messages' => $response]);
    }
}